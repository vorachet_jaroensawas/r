# Tasks

## Import Data

### Import Table File

Create text file data.txt
```
100   a1   b1
200   a2   b2
300   a3   b3
400   a4   b4
```

Call R from the data.txt directory
```
$ r
```

command to read text file and save data into dataframe named data
```
> data = read.table("data.txt")
```

command to print data
```
> data
  V1 V2 V3
1 100 a1 b1
2 200 a2 b2
3 300 a3 b3
4 400 a4 b4
```
